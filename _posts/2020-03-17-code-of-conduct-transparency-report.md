---
title: Code of Conduct Transparency Report
date: 2020-03-16 00:00:00 -0400
excerpt_separator: <!--more-->
---

PyGotham strives to be a friendly and welcoming environment, and we take our
code of conduct seriously. In the spirit of transparency, we publish a summary
of the reports we received this year.

<!--more-->

We appreciate those reports, and we encourage anyone who witnesses a code of
conduct violation to report it via the methods listed at
<{{ site.url }}{% link about/code-of-conduct.md %}>. PyGotham 2019 had four
code of conduct reports made to the organizers. Anonymized details of these
reports are below.

- During the public voting phase of PyGotham’s talk selection process, a voter used the code of
  conduct report form to report an issue unrelated to the code of conduct. Conference staff
  confirmed that this was not a code of conduct incident with the reporter, and no further action
  was taken.

- During the public voting phase of PyGotham’s talk selection process, a report was made regarding
  possible usage rights in a data collection process covered by the proposed talk. Copyright and /
  or terms of service violations are out of scope for PyGotham’s code of conduct. No further action
  was taken.

- An attendee made a joke about a turtle with no legs. Another attendee approached them to let them
  know that they should not have made the joke and should apologize. The first attendee reported
  themself and apologized.

- The Code of Conduct committee received a report that a vendor had violated a professional code of
  conduct. Conference staff agreed that this did not violate PyGotham’s code of conduct, and
  communicated this with the reporter. No further action was taken.
