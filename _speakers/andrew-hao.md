---
name: Andrew Hao
talks:
- "Can Neural Networks Help Me Become a Better Parent?"
---

Andrew is a sleep-deprived dad who's heavily into running, photography and making music. He currently works at Lyft, building growth systems that help acquire and onboard new riders and drivers. Prior to that, he led teams that built software for clients big and small at Carbon Five.

Whenever he's not pounding the pavement, you can find him on the living room floor working on puzzles with his son.