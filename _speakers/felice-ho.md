---
name: Felice Ho
talks:
- "None, null, nil: lessons from caching and representing nothing with something"
---
Felice Ho is a Data Engineer at Equinox Fitness. She builds backend services, data integrations, and data platform tools, as well as contributes to open source packages. Felice is also an organizer for NYC PyLadies.
