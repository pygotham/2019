---
name: Samuel Roeca
talks:
- "Poetry: \"dependency management and packaging made easy\""
---

Sam is the Head of Engineering at Kepler Group LLC. He also organizes The New
York City Vim Meetup. Sam grew up on Oahu but now lives in New York City. He
loves designing and building software with Python, Linux, Vim, and a whole host
of other tools.