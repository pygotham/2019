---
name: Christopher Wilcox
talks:
- "The blameless post mortem: how embracing failure makes us better"
---
Chris is a developer at Google in Seattle, WA, USA and works on the Google
Cloud Platform Client Libraries team, focusing on dynamic languages and
their users. Before joining Google, Chris spent 6 years working on
compilers, cloud platforms, and developer tooling for Microsoft. He brings a
passion for enabling developers everywhere through better tools and
libraries. In his spare time you can find Chris running or cycling nowhere
in particular or riding his motorcycle.
