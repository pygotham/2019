---
name: Ridhi Kapoor
talks:
- "Interact with your code: Using interactive widgets in Jupyter Notebooks"
---
Ridhi Kapoor has worked as a data scientist at Bloomberg since 2017. She
works in the BVAL Quant group, where her team develops models to price fixed
income securities. She holds a bachelor's degree in Electrical Engineering
from the Indian Institute of Technology Delhi (IIT-Delhi). She is currently
spending a part of her time pursuing a master's degree in Data Science at
Columbia University.
