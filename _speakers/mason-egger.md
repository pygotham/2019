---
name: Mason Egger
talks:
- "Building Docs Like Code: Continuous Integration for Documentation"
---
Mason Egger is a Developer Advocate at DigitalOcean. He is an avid programmer, 
musician, educator, and writer/blogger at [mason.dev](https://mason.dev). 
In his spare time he enjoys camping, kayaking, and exploring new places.
