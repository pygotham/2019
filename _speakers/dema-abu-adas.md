---
name: Dema Abu Adas
talks:
- "The Adventures of A Python Script"
---
Dema is a 4th year software engineering student at the University of Guelph,
from Ontario, Canada. She has a weird love for low level concepts and enjoys
developing in Python whenever she has the chance. In her free time, Dema
enjoys rock climbing as well as organizing computing related events!
