---
duration: 25
presentation_url: https://docs.google.com/presentation/d/1J2dHAvxcKakUt9knwqJr-7RqAGkf2FTpt0TxoLcYQbA/edit?usp=sharing 
room: PennTop North
slot: 2019-10-05 14:30:00-04:00
speakers:
- Neehaarika Velavarthy
title: Analyzing the Evolution of Irish Traditional Music using Python
type: talk
video_url: https://youtu.be/iUMyanuM2gI
---

Celtic music, and more specifically, Irish music has been around for at
least a few hundred years now. Being folk music, most of these tunes have
been passed down the generations by ear causing many variations of the same
tune to co-exist. Even today, as the tunes travel geographically, **they
develop slight variations** according to the players' individual quirks.

Over recent times, websites like [thesession.org](https://thesession.org/)
are beginning to collect well-known tunes in databases made up of player
contributions. This particular website features both the *abc* and *midi*
formats in addition to the staff notation. Players are allowed to contribute
their own versions of popular tunes which makes this site very conducive to
observing **patterns in the variations**. This analysis will explore **how
these tunes changes across their newer versions and what makes some tunes
change more than the others**. It will define metrics by which tune
variations can be quantified and visualized and it will try to unearth
common patterns in these variations.
