---
duration: 25
presentation_url: https://github.com/judy2k/make_you_an_async
room: PennTop North
slot: 2019-10-05 10:45:00-04:00
speakers:
- Mark Smith
title: Make You An Async For Great Good!
type: talk
video_url: https://youtu.be/XEkuqe7tSlA
---

AsyncIO seems like magic, but it's (mostly) not! I'm going to write the core
of asyncio from scratch. I will explain how asyncio actually works in pure
python, building up from generators and a custom event loop.

This talk will demystify the `async` and `await` keywords, and help
developers write better asynchronous code!