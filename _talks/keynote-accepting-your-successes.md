---
duration: 30
presentation_url: https://github.com/pathunstrom/accepting-your-success
room: PennTop South
slot: 2019-10-05 09:30:00-04:00
speakers:
- Piper Thunstrom
title: Accepting your successes
type: keynote
video_url: https://youtu.be/_HzbJ6UjN4A
---

Like many people, Piper has struggled with accepting her competencies. It
was hard to ignore the many resounding failures in her life, until she
looked up one day, and realized she'd met all the markers of her childhood
dreams. Now what to do with "success"?
