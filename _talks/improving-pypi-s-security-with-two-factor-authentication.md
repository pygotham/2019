---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-05 10:45:00-04:00
speakers:
- William Woodruff
title: Improving PyPI's security with Two Factor Authentication
type: talk
video_url: https://youtu.be/xNZIxt-ABUs
---

Since March, [Trail of Bits](https://www.trailofbits.com) has worked with
the PSF to implement and land major security improvements in
[Warehouse](https://github.com/pypa/warehouse/), the codebase that drives
[PyPI](https://pypi.org). This talk will cover just one of those
improvements: the addition of two factor authentication to user logins.
Attendees will learn about the technical details of two factor schemes, the
security properties they can (and can not) provide, and the process for
making major changes to core Python infrastructure.
