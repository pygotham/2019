---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-05 14:00:00-04:00
speakers:
- Susan Sun
title: How to size up a Python project for data freelancing
type: talk
video_url: https://youtu.be/oCuHq8L5rao
---

One of the most dreaded questions a freelancer hears is "Okay, so how long
do you think this will take you?"  Without tons of experience and deep self-
awareness, it's always hard to size up a data project.  Even with
experience, sometimes there are spectacular failures.  Let's have a fun
discussion about a few key ways to size up a project, including how to ask
good scoping questions, how to budget for unexpected and time-consuming
scenarios.  The discussion will also be liberally sprinkled with personal
anecdotes of scoping failures from my career as a data freelancer.
