---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-05 10:15:00-04:00
speakers:
- Casey Faist
title: 'PPB: Unbearably Fun Game Development'
type: talk
video_url: https://youtu.be/zM9G7wAf2g0
---

Want to make your own games, but want building the game to also be fun? Or,
know a teacher looking to bring game development into the classroom?

Come learn about the coolest new framework, PursuedPyBear. Driven by
dataclasses and designed to give a Pythonic development experience, it
brings the joy back into video game development!
