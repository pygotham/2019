---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-04 14:45:00-04:00
speakers:
- Nabeel Seedat
title: 'Introduction to Generative Adversarial Networks (GANs): hands-on to making
  new data (and some pretty pictures)'
type: talk
video_url: https://youtu.be/cLWvypb2lR0
---

Deepfakes has caused a huge stir over the past few years. **Generative
Adversarial Networks (GANs)** are the cause of it all. GANs are a type of
**deep neural network** that can generate new data. GANs comprise of two
competing networks and the results, especially in creating new images are
truly astounding.

This talk will introduce deep learning and how GANs work, highlighting
application areas. Most importantly it will provide a **hands-on
implementation** of GANs using **Pytorch**. With the **tips and tricks**
from this talk, you’ll be ready to start experimenting with GANs!
