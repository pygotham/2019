---
duration: 25
presentation_url: https://speakerdeck.com/andrewhao/can-neural-networks-make-me-a-better-parent
room: PennTop South
slot: 2019-10-04 15:15:00-04:00
speakers:
- Andrew Hao
title: Can Neural Networks Help Me Become a Better Parent?
type: talk
video_url: https://youtu.be/sT_yS8XAQEw
---

When nighttime descends, our household becomes a battleground of sleep battles with our toddler (a total bummer!) How can building a TensorFlow-powered cry-detection baby monitor help me understand my little one?

If you are a beginner or just curious about machine learning, this talk is for you. Together, we'll go on a ML discovery journey to train a TensorFlow model to recognize and quantify the cries of my little one. We'll see how it works by walking through a keyword-spotting CNN described from a Google research paper, then see how it's deployed on a Raspberry Pi. In the process, you'll see how simple it is to train and deploy an audio recognition model!

Will the model deliver on its promise to deliver sleep training insights to this sleep-deprived parent? And what can training a model on human inputs teach us about building production models in the real world?