---
duration: 25
presentation_url: https://www.slideshare.net/twitterdev/how-i-solved-my-nyc-parking-problem-with-python-178892380
room: PennTop South
slot: 2019-10-04 13:00:00-04:00
speakers:
- Jessica Garson
title: 'How I Solved my NYC Parking Problem with Python '
type: talk
video_url: https://youtu.be/pgDu37WtDhk
---

Since I have a car in New York City, my car is subject to the city’s
alternate side of the street parking regulations. This means most nights I
need to move my car before the early morning street cleaning that happens in
my neighborhood. I had developed a nightly routine around moving my car
before I go to bed. I am sometimes a bit too good at this and I often move
my car on days I don’t need to. Since alternate side of the street parking
is often canceled on days where there are holidays, or bad weather, there is
a Twitter handle @NYCASP, which posts daily and whenever there is an
emergency situation. I used Python, Twilio and the Twitter API to solve this
problem for myself so I get a text message whenever I don't need to move my car.