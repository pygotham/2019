---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-04 15:45:00-04:00
speakers:
- Ethan Cowan
title: Ethics & Bias Detection in the Real World
type: talk
video_url: https://youtu.be/FkCb7345Hj8
---

As algorithmic decision makers become more pervasive in day-to-day life,
there is an increased urgency to address inherent biases and their potential
for perpetuating societal inequities.  We will outline new and existing
methods for determining implicit bias in machine learning models and rating
systems.
