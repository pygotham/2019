---
duration: 25
presentation_url: https://www.slideshare.net/zothcorp/recreating-the-clock-with-machine-learning-and-web-scraping
room: Madison
slot: 2019-10-04 13:00:00-04:00
speakers:
- Kirk Kaiser
title: Recreating "The Clock" with Machine Learning and Web Scraping
type: talk
video_url: https://youtu.be/zaPqpN5hrfU
---

"The Clock" is a 2010 art installation by Christian Marclay. It is an
experimental film that features over 12,000 individual shots of clocks from
movies and television, edited in such a way that the film itself functions
as a clock.

In this talk, we'll use modern machine learning models and video web
scraping to recreate the concept behind "The Clock". We'll use Kubernetes to
orchestrate building a modern video scraper, capable of getting around the
walls of YouTube and Instagram to grab user content.

We’ll then use existing machine learning models to infer when clocks occur
in videos, to create our own montage with the found internet video.

Along the way, we'll learn how to build and monitor machine learning
services meant to scale from our local computers to hundreds of machines.