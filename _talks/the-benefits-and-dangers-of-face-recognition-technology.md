---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-04 14:45:00-04:00
speakers:
- Manojit Nandi
title: The Benefits and Dangers of Face Recognition Technology
type: talk
video_url: https://youtu.be/nJ9LZMoT5Rc
---

Biometric scanners, such as face recognition technology, have seen
widespread adoption in applications, such as identifying suspected
criminals, analyzing candidate's facial expressions during job interviews,
and [monitoring attendance at church](https://churchix.com/).

As these technologies have become more pervasive, many organizations have
raised potential concerns about the way these technologies schematize faces.
Studies have shown commercial face recognition software has noticeably lower
accuracy on darker-skinned individuals, and automatic gender recognition
systems regularly misgender trans and non-binary individuals.

In this talk, I will cover the known limitations of face recognition
technology and the privacy concerns raised about these systems. At the end,
I will cover how companies and government legislation aims to address these
concerns.
