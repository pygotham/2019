---
duration: 40
presentation_url: null
room: PennTop South
slot: 2019-10-04 09:20:00-04:00
speakers:
- Kojo Idrissa
title: The Python Use Spectrum
type: keynote
video_url: https://youtu.be/TOG6bmt6SrM
---

Python uses exist on a spectrum from personal problem solving to professional
software engineering. While the former has grown faster, fueling our
community's growth, many of our cultural norms are biased towards the
latter. Our ONLY potential contributors come from this spectrum, so how do
we maximize new contributors? And why should we care?
