---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-04 13:30:00-04:00
speakers:
- Veronica Hanus
title: To comment or not? A data-driven look at attitudes toward code comments
type: talk
video_url: https://youtu.be/48lQubw3Avw
---

While most of us agree that commenting is part of writing maintainable code,
it’s very difficult for someone who has not yet worked in a community-
reviewed codebase to know what is good practice and not. The answers that
come back often conflict each other: Code should be DRY, but well-placed
comments save future devs. How can someone find the commenting style that is
best for them as they learn, grow, & contribute? My survey of 170 long-time
developers, Computer Science majors, bootcamp grads, & hobby programmers
confirms some expectations and brings others into question. Join me for a
data-based chat about the biggest pain points caused by our attitudes toward
commenting and the steps we can take to encourage a growth mindset and
empower programmers of all levels.
