---
duration: 25
presentation_url: https://masonegger.com/talks/ci-docs/
room: PennTop North
slot: 2019-10-04 13:00:00-04:00
speakers:
- Mason Egger
title: 'Building Docs Like Code: Continuous Integration for Documentation'
type: talk
video_url: https://youtu.be/wEt_8twQctQ
---

# Abstract

Project documentation is easy to neglect. Keep your docs inside your
source repo & learn how to automatically build & publish beautiful docs
on every commit. Viewers will leave with a new mindset on how to handle
documentation, tooling for this process, & an easy-to-implement method
to achieve this.

# Description

It is common for developers to overlook the documentation of their
works. They are either on a time crunch, lack the proper tooling, or
simply just forget to create and update the documentation. Whatever the
cause behind this, it is not a proper excuse for not keeping the
documentation up to date. However, for all our development processes
there are few as neglected as the documentation process. Documentation
should be treated as important as the code that makes up the project.
So, let’s move the documentation into the code. With modern
documentation tools such as MkDocs and Sphinx, both of which are Python
powered tools, and Continuous Integration tools we can now include docs
in the commit. They can be reviewed in code reviews, built and versioned
in a CI tool, and even tested for things such as correct code examples
and broken links. This is the process that the developer knows,
understands, and enjoys. I introduced a team to this exact workflow and
a working pipeline; all they had to do was keep the documentation up to
date. This team currently has some of the most up to date documentation
in a company of near two thousand engineers, and they never complain
about writing/updating documentation. It’s just part of the workflow.