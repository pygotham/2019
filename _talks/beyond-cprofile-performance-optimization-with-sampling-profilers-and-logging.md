---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-05 15:15:00-04:00
speakers:
- Itamar Turner-Trauring
title: 'Beyond cProfile: performance optimization with sampling profilers and logging'
type: talk
video_url: https://youtu.be/fOzVTPOWfQs
---

Your software is too slow, and you need to figure out why. It's natural to
reach for the cProfile profiler that's built-in to Python, but as a
deterministic profiler it suffers from a number of limitations.

In this talk you'll learn about better tools for performance optimization:

* Sampling profilers, which distort the results less and give you a calltree
  overview of your running application.
* Trace-based logging, which can help you figure out not only which
  functions are slow, but also which particular inputs are causing the
  slowness.
