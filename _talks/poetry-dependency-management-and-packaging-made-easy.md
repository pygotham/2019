---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-05 13:30:00-04:00
speakers:
- Samuel Roeca
title: 'Poetry: "dependency management and packaging made easy"'
type: talk
video_url: https://youtu.be/QX_Nhu1zhlg
---

Learn how to write a Python library, fearlessly manage its dependencies, and
distribute it to the world with
[Poetry](https://github.com/sdispater/poetry), a tool dedicated to making
"dependency management and packaging ... easy".

Have you struggled with setuptools, Pipenv, pip, requirements.txt, and
finally just given up on your dreams of distributing your awesome new
artificial intelligence / NLP library? Do you think there should be a better
way to initialize, evolve, and eventually share your awesome library or
application? Have you played around with Rust and wondered whether Python
would ever get something as beautiful as Cargo for itself? If any of these
questions describes you, then let me tell you about Poetry...
