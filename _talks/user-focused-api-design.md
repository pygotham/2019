---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-04 14:00:00-04:00
speakers:
- Renato Oliveira
title: User focused API design
type: talk
video_url: https://youtu.be/bp3Spc_sNL8
---

When we talk about Web API Design, we're usually driven to think in
architecture, verbs and nouns. But we often forget our user: the developer.

UX designers rely on many techniques to create great experiences. User
research, User Testing, Personas, Usage Data Analysis and others. However
when creating `invisible products` we’re not used to think in usability. So
why don’t we take advantage of this background to improve our APIs
experiences?

User experience refers to a person’s emotions and attitudes about using a
particular product, system or service. If your API is poorly designed,
documented or hard to start using and there is no requirement for the
developer to use it, they won’t. In fact, it’s more likely for them to build
something from scratch to replace your API rather than use it.

In this talk, I'll show some processes commonly used by UX designers and how
we can use them to create a great developer experience.
