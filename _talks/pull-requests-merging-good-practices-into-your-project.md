---
duration: 25
presentation_url: https://drive.google.com/file/d/1qHMsrpsGzi-N9XhIKKTRXMmq2jiXIffc/view
room: PennTop South
slot: 2019-10-05 13:00:00-04:00
speakers:
- Luca Bezerra
title: 'Pull Requests: Merging good practices into your project'
type: talk
video_url: https://youtu.be/DuectEtpWvY
---

Although known by most, Pull Requests are often not dealt with in the most
effective way. Believe it or not, there are teams that don’t review code at
all! People may assume that a senior developer is experienced enough to not
make any mistakes, or that merely changing those 3 lines of code couldn’t
possibly do any harm to the system. In these cases, it’s not uncommon to
skip the code review in order to cut some time. Unreviewed (or badly
reviewed) code can be extremely dangerous, resulting in huge risks and
unpredictable behavior.

A survey says that, on average, developers spend 45% of their time fixing
bugs and technical debt, when they could be developing new features instead.
Defining simple guideline files, adopting certain behaviors and setting up
repository configurations are steps that can increase manyfold the code
review performance (in both time and quality). Using review tools both on
server (e.g. Heroku Review Apps) and locally (e.g. linters) can also greatly
increase the process’ speed. Creating templates and checklists ensures no
step is overlooked or forgotten. The list goes on, but enough spoilers for
now. The attendees will learn specific tips, tools, processes and
recommended practices that were compiled from research and real-life use
cases (both from my experience and from big players like Django, Facebook,
Mozilla, etc), along with some survey data that demonstrates why reviewing
code is important.