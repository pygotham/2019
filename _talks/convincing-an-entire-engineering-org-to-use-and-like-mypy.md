---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-04 16:55:00-04:00
speakers:
- Annie Cook
title: Convincing an entire engineering org to use (and like) mypy
type: talk
video_url: https://youtu.be/S75VVAZmqZ8
---

Many engineers’ first encounter with a static type-checker is being told off
by it: you roll your eyes and mypy shouts ERROR. Really, why invest your
precious time in mypy at all? This is my conversion story, from disgruntled
engineer to mypy believer. I’ll share with you the good gospel of its
benefits, strategies to increase buy-in from your team and tips for how to
add mypy into your workflow.

In a dynamically typed language like Python, it is worth asking, “Why use
mypy and invest precious engineering time to add and check types?” My time
as an engineer at Nylas has seen an engineering org-wide conversion to mypy,
for good reason. I'll share how we were able to gradually increase type
coverage in our entire codebase from 0% to 80%.

Warming to the change took effort, but the substantial benefits to the
quality and sustainability of our codebase have made me into a mypy
believer. Even so, becoming a believer is only the first step. Next, you
have to inspire your fellow engineers to join your mypy quest. How can you
convince your team to want to put in this extra work? I'll present
strategies for expanding buy-in from your fellow engineers and processes to
ensure that your type coverage does not regress. My story may just make
believers out of you and your team.
