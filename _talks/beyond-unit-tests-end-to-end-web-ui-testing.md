---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-04 15:15:00-04:00
speakers:
- Andrew Knight
title: 'Beyond Unit Tests: End-to-End Web UI Testing'
type: talk
video_url: https://youtu.be/Z9ghRBEgnps
---

Unit tests are great, but they don’t catch all bugs because they don’t test
features like a user. However, Web UI tests are complicated and notoriously
unreliable. So, how can we write tests well? Never fear!

Let’s learn how to write robust, scalable Web UI tests using Python, pytest,
and Selenium WebDriver that cover the full stack for any Web app. In this talk,
we will write one simple test together that covers DuckDuckGo searching.
We’ll cover:

* Using Selenium WebDriver like a pro
* Modeling Web UI interactions in Python code
* Writing “good” feature tests that are efficient, robust, and readable
* Deciding what should and should not be tested with automation


After this talk, you’ll know how to write battle-hardened Web UI tests for
anyWeb app, including Django and Flask apps. I’ll provide the example code on
GitHub, as well as plenty of hands-on tutorials and resources to continue
learning after this talk.
