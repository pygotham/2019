---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-04 16:25:00-04:00
speakers:
- Abid H. Mujtaba
title: 'Memoizing recursive functions: How decorators are more elegant than I imagined'
type: talk
video_url: https://youtu.be/OAajxA2qjIU
---

Calculating the Fibonacci series is a classic example of writing a recursive
function. However, calculating even `fib(50)` will tie up a modern computer
for a couple of minutes. The solution to this problem is memoization, which
decorators make particularly easy.

But how does a top-level decorator access the recursive call within the
function itself?

The answer is both delightfully simple and extremely insightful, exposing
how seemingly simple language design choices can have profound effects on
its usage.
