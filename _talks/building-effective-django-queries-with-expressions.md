---
duration: 25
presentation_url: https://docs.google.com/presentation/d/1HhISje4VyaQcjElRDsrKkZpIMuJQy9uLbGhfkuVswDI/edit?usp=sharing
room: Madison
slot: 2019-10-04 14:45:00-04:00
speakers:
- Vanessa Barreiros
title: Building effective Django queries with expressions
type: talk
video_url: https://youtu.be/oGhO8aa5Wbk
---

It's known that ORMs are a powerful tool to manipulate databases with ease.
In Django, there are a set of out-of-the-box abstractions to help perform
queries and shape them through annotations, aggregations, order by, and so
on, hence saving one's time. A common solution to filtering when models grow
larger over time is creating redundant fields; a better solution is using
Django built-in resources called query expressions.

Query expressions are smart yet straightforward functions that one can use
to compute values on query execution and do string manipulation,
calculations, among others, thus removing the burden of having unnecessary
extra columns in our database. Using query expressions effectively can help
to generate performant queries, avoiding potential inconsistencies and
separating concerns.

This talk focuses on further optimizing Django queries by walking through
code comparisons and examples with a dataset, diving into subjects such as
custom database functions, conditional expressions, and filtering so to
answer questions about the data.