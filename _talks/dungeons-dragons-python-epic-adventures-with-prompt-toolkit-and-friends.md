---
duration: 40
presentation_url: https://speakerdeck.com/mpirnat/dungeons-and-dragons-and-python-epic-adventures-with-prompt-toolkit-and-friends-cb758e08-1300-4592-9cbd-34a595fd60ca
room: PennTop North
slot: 2019-10-05 11:15:00-04:00
speakers:
- Mike Pirnat
title: 'Dungeons & Dragons & Python: Epic Adventures with Prompt-Toolkit and Friends'
type: talk
video_url: https://youtu.be/TjUTaFcxXYo
---

Embark on an epic adventure through the twisty passageways of [a Python
application developed to help Dungeon Masters run Dungeons & Dragons
sessions](https://github.com/mpirnat/dndme). You’ll be joined in your quest
by mighty allies such as [Prompt-Toolkit](https://python-prompt-
toolkit.readthedocs.io/en/master/),
[Attrs](https://www.attrs.org/en/stable/),
[Click](https://click.palletsprojects.com/en/7.x/), and
[TOML](https://pypi.org/project/toml/) as you brave the perils of
application structure, command completion, dynamic plugin discovery, data
modeling, turn tracking, and maybe even some good old-fashioned dice
rolling. Treasure and glory await!