---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-04 10:15:00-04:00
speakers:
- Keenan Szulik
title: 'Open source sustainability: what does this mean for Python?'
type: talk
video_url: https://youtu.be/VEV1u6IyWQA
---

Across open source communities—from Python to JavaScript to Ruby—businesses
and developers alike are wondering how we can better sustain the critical
infrastructure that open source projects have become. This talk will survey
different approaches within the Python community (including foundations,
startups, individual maintainer efforts), why they're different, and how/why
you should get involved!
