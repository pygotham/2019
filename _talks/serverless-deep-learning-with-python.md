---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-05 15:45:00-04:00
speakers:
- Alizishaan Khatri
title: Serverless Deep Learning with Python
type: talk
video_url: https://youtu.be/cFld9N7yn6A
---

Do you marvel at the idea of production-grade Deep Learning that can scale
infinitely in nearly constant time?

Using an NLP application as an example, you will learn how to design, build
and deploy deep learning systems using a serverless computing platform
(Function As A Service).

PS: Talk contains code!
