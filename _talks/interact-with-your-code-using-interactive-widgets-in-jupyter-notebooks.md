---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-05 15:45:00-04:00
speakers:
- Ridhi Kapoor
title: 'Interact with your code: Using interactive widgets in Jupyter Notebooks'
type: talk
video_url: https://youtu.be/StvKr-1o3mM
---

One of the most important parts of a data scientist’s job is to be able to
visualize and get insights from data. What if there was a simpler way to
explore data? What if we could change input variables simply by sliding a
mouse or we could control the degree of the polynomial by selecting from a
drop-down menu, and then see the output change instantaneously? This would
allow data scientists to quickly prototype their ideas. Interactive widgets
in Jupyter Notebooks allow one to do exactly that. The interact function
(ipywidgets.interact) automatically creates user interface controls to
explore code and data interactively. In this talk, I will highlight the
powerful capabilities that widgets and interactive functions provide, and
how one can efficiently leverage them to build interactive Jupyter
Notebooks.
