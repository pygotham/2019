---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-04 16:55:00-04:00
speakers:
- Rob Spectre
title: Using NLP To Reduce Wealth Flowing Into Local Human Trafficking Economies
type: talk
video_url: https://youtu.be/H2YrkQGRBc8
---

With $150 billion generated in profit a year, human trafficking is an
illicit business model that affects children and adults in every community
in the United States. Any strategy to reduce the prevalence of the crime
must include reducing the amount of profit. Working with the New York Police
Department, I built a prototype using NLP to target the buyers contributing
the wealth to New York's human trafficking economy. That prototype went on
to serve nine jurisdictions across the United States and reach tens of
thousands of buyers.

This talk explores the development of the chatbot engaging with buyers every
hour of every day across the country to reduce the economic incentives
behind human trafficking. We'll explore scoping the domain of an NLP
problem, designing the right machine learning model to address it, and
creative ways to collect sufficient data to train an effective model.
