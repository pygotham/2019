---
duration: 25
presentation_url: "https://docs.google.com/presentation/d/1YmPzG4p6ejUjSe6lfclUbsPtPLJmSEpH06THAXu5OIc/"
room: PennTop North
slot: 2019-10-05 15:45:00-04:00
speakers:
- Dema Abu Adas
title: The Adventures of A Python Script
type: talk
video_url: https://youtu.be/KkwBlOTnXgA
---

Have you ever wondered what happens between the time you run helloWorld.py
and the terminal prints out “Hello world”? I will be sharing the wonderful
and interesting process of how the Python interpreter works from the Python
source code to the compilation of bytecode.

Steve Yegge, a programmer and blogger who has a plethora of experience in
operating systems, once noted the importance about compilers by stating, “If
you don’t know how compilers work, then you don’t know how computers work”.
This talk will share an overview of how CPython works from lexxing to
compiling as well as how the abstract syntax tree (AST) works. At the end of
the talk, you’ll be able to understand the general concept of the abstract
syntax tree (AST) and how creating a interpreter can additionally benefit
you in ways unrelated to the actual compilation such as linting and
debugging.
