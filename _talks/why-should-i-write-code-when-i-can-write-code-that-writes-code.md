---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-05 10:15:00-04:00
speakers:
- James Powell
title: Why should I write code when I can write code that writes code?
type: talk
video_url: https://youtu.be/tZQ0U_knMlM
---

The temptation to employ code-generating techniques in Python is strong.
Much of what is called "metaprogramming" in Python refers to the various
techniques through which we can write higher-order code: code that then
generates the code that solves our problem. This talk discusses various
common approaches for code-generation, their applicability to solving real
problems, and the reality of using these techniques in your work.
