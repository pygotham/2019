---
duration: 25
presentation_url: https://docs.google.com/presentation/d/1O4UjUFL39CIKcAXvIz5SUO_Yi1uj8Csc7EAplp1T-4Y/edit?usp=drivesdk
room: PennTop North
slot: 2019-10-04 10:15:00-04:00
speakers:
- Hugo Bessa
title: Taming Irreversibility with Feature Flags
type: talk
video_url: https://youtu.be/BAzbW4WkteE
---

It's been 10 years since Flickr’s development team documented the use of
Feature Flags in their software. Tech giants like Google and Facebook have
also stated their use, yet weirdly enough there seems to be only but few
around the community benefitting from feature flipping.

Flags make toggling whole features on and off without touching the code
possible. This can help the development team not only by cutting down on
response time to disasters but also by loading on peace of mind for
developers. There are also great improvements on code sync frequency and in
the launching flow of new features - especially in applications with a large
sum of users.

Along with these great benefits, feature flags also raise some concerns:
there are multiple strategies to implement them and numerous new things to
worry about when developing new gated features. From the tools you can use
to store and retrieve your flags to the way you can maintain your
application’s consistency in edge cases scenarios.

This talk focuses on some of the benefits and challenges faced when using
feature flags on team projects, and how to extract their best value without
losing sight of code quality.