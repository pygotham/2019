---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-05 13:30:00-04:00
speakers:
- Jacob Deppen
title: "Sprints - Let\u2019s do more of those!"
type: talk
video_url: https://youtu.be/8Wc5MkUdp-4
---

Few people attend conferences just to attend the associated sprints.
However, I want to tell you a story about how sprints offer a unique space
for certain kinds of ideas and collaboration that are otherwise difficult or
impossible to cultivate.

I will begin at the end, by describing the `pandas-vet` plugin for the
`flake8` linter. I will briefly summarize some elements underlying the
`flake8` plugin (`pandas` best practices, traversing the AST, and
entrypoints) as well as elements of a good open-source project (tests, CI,
pip-installable, contributor guidelines, and a code of conduct).

Next, through Slack messages and GitHub commits, I will tell the story of
how the sprinting environment helped the project go from vague idea to
implementing all of those elements in just one day.

Throughout the talk, I'll offer some of the lessons we should all learn
about what makes sprints unique, what makes them successful, and how this
project could only have been built in a sprint environment.
