---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-04 13:30:00-04:00
speakers:
- Ryan Kelly
title: Advanced SQL with SQLAlchemy
type: talk
video_url: https://youtu.be/UPoHdCeg0YQ
---

SQL is an incredibly powerful way to access your data, and SQLAlchemy’s
flexibility allows you to harness all this power in a Pythonic way.
