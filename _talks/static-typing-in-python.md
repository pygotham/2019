---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-04 16:25:00-04:00
speakers:
- Dustin Ingram
title: Static Typing in Python
type: talk
video_url: https://youtu.be/2gBP1qN5T7I
---

Python is well-known as a programming language without static types. This
means that you don't need to say what a given variable will hold, or whether
your function will return a string or an integer (or sometimes one, and
sometimes another!). This has historically made Python a very flexible and
beginner-friendly language.

In this talk, we'll discuss the advantages and disadvantages to a static
type system, as well as recent efforts to introduce static typing to Python
via optional "type hints" and various tools to aid in adding types to Python
code. We'll see what this means for Python, for Python programmers, and what
the future has in store for Python's type system.
