---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-05 13:00:00-04:00
speakers:
- Moshe Zadka
title: A Time Traveler's Perspective on Software Developer Code of Ethics
type: talk
video_url: https://youtu.be/UuDxiGxM2CQ
---

In the history part in "Ethics for Software Developer", I have heard that
there was no code of ethics for software developers in the 21st century. Now
that I have time-traveled here from the 23rd century, I realized it was
true. I will share the accepted wisdom from the 23rd century about how code
of ethics and licensing looks like.

Content Warning: There will be brief mentions of pornography
and pornography-centric applications.
