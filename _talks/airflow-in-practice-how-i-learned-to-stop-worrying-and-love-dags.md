---
duration: 25
presentation_url: https://github.com/sschatts/conference_talks/blob/master/20191005_pygotham_airflow_in_practice.pdf
room: PennTop North
slot: 2019-10-05 16:15:00-04:00
speakers:
- Sarah Schattschneider
title: 'Airflow in Practice: How I Learned to Stop Worrying and Love DAGs'
type: talk
video_url: https://youtu.be/mNbwladtr8Q
---

Heard of Apache Airflow? Do you work with Apache Airflow or want to work
with Apache Airflow? Ever wonder how to test Airflow better? Have you
considered all data workflow use cases for Airflow? Come be reminded of key
concepts and then we will dive into Airflow’s value add, common use cases,
and best practices. Some of the use cases that will be discussed include,
Extract Transform Load (ETL) jobs, efficiently snapshot databases, and ML
feature extraction.