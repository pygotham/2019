---
duration: 25
presentation_url: null
room: PennTop North
slot: 2019-10-05 13:00:00-04:00
speakers:
- Katie McLaughlin
title: 'What is deployment, anyway? '
type: talk
video_url: https://youtu.be/S7dOCEBoX1w
---

So you’ve finished the DjangoGirls tutorial, but now you want to share it
with the outside world, but how do you go about that?

In this presentation, we will discuss the basics beyond running a django
project locally, and discuss the concepts and strategies around how to host
your project.
