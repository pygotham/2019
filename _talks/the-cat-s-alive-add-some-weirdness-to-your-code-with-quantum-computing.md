---
duration: 25
presentation_url: null
room: PennTop South
slot: 2019-10-05 14:00:00-04:00
speakers:
- Jorn Mossel
- Marianne Hoogeveen
title: The Cat's Alive! Add Some Weirdness To Your Code With Quantum Computing
type: talk
video_url: https://youtu.be/XvdVw6f9pnc
---

Dear 10x programmer. Aren’t you bored of writing code that always works, and
gives you the exact result you want? Don't lose hope, try a quantum
computer! Regardless of how good your code is (and we’re talking about a
brand new programming language), you will never get it fully under control,
and that's the beauty. Let us show you how mount this untamable quantum
beast.

Starting with the basics of a quantum computer (which unfortunately doesn't
consist of a bunch of Schrodinger's cats doing computations, either alive or
dead), we will explain why people think there might be problems where this
new evolution of computation will reign supreme in the future, and how we
can use it to find good approximate solutions to complex problems in the
meantime. We will talk about the new wave of development in quantum hardware
and software, but the cherry on the cake will be running a computation on
actual quantum hardware - live.
