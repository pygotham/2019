---
duration: 25
presentation_url: https://docs.google.com/presentation/d/17xHdStRppvwYqbPggh_4rzLGAoFm2wFcbNKXehJs0LE/edit#slide=id.g3522410578_0_0
room: PennTop North
slot: 2019-10-04 14:00:00-04:00
speakers:
- Michael Lynch
title: Why Good Developers Write Bad Tests
type: talk
video_url: https://youtu.be/ElzBGwyDzCc
---

Most developers still approach tests with the same old techniques they use
for production code. But test code is not like other code. To write it well,
we need to reexamine the principles and goals that define our best
practices.

In this talk, I'll discuss:

* What separates good tests from bad ones
* Why you should think twice before refactoring test functions
* How to identify anti-patterns in tests that hide bugs
* Why your test names are probably too short
* Why you should embrace magic numbers